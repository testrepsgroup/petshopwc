import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    WebClientMyEmployees = "WebClientMyEmployees",
        Table1 = "Table1",
        Column8 = "Column8",
        EmployeeID = "EmployeeID",
        Column9 = "Column9",
        FirstName = "FirstName",
        Column10 = "Column10",
        LastName = "LastName",
        Column11 = "Column11",
        Title = "Title",
        Column12 = "Column12",
        DeptID = "DeptID",
        Column13 = "Column13",
        ManagerID = "ManagerID",
}
export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get EmployeeID(): FormControl {
        return this.getTableChildFormControl(MgControlName.EmployeeID);
    }

    get FirstName(): FormControl {
        return this.getTableChildFormControl(MgControlName.FirstName);
    }

    get LastName(): FormControl {
        return this.getTableChildFormControl(MgControlName.LastName);
    }

    get Title(): FormControl {
        return this.getTableChildFormControl(MgControlName.Title);
    }

    get DeptID(): FormControl {
        return this.getTableChildFormControl(MgControlName.DeptID);
    }

    get ManagerID(): FormControl {
        return this.getTableChildFormControl(MgControlName.ManagerID);
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}