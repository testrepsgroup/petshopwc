import {
    Component
} from '@angular/core';

import {
    FormGroup
} from "@angular/forms";
import {
    MgFormControlsAccessor,
    MgControlName
} from "./WebClientMyEmployees.mg.controls.g";


import {
    BaseMatTableMagicComponent,
    matMagicProviders
} from "@magic-xpa/angular-material-core";


@Component({
    selector: 'mga-WebClientMyEmployees',
    providers: [...matMagicProviders],
    templateUrl: './WebClientMyEmployees.component.html'
})
export class WebClientMyEmployees extends BaseMatTableMagicComponent {

    mgc = MgControlName;
    mgfc: MgFormControlsAccessor;
    createFormControlsAccessor(formGroup: FormGroup) {
        this.mgfc = new MgFormControlsAccessor(formGroup, this.magicServices);
    }

    displayedColumns = [
        'Column8',
        'Column9',
        'Column10',
        'Column11',
        'Column12',
        'Column13',
    ];

}