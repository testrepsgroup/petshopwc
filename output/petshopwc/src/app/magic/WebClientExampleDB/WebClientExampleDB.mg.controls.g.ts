import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    WebClientExampleDB = "WebClientExampleDB",
        Table1 = "Table1",
        Column5 = "Column5",
        ID = "ID",
        Column6 = "Column6",
        VALUE = "VALUE",
        Column7 = "Column7",
        LETTER = "LETTER",
}
export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get ID(): FormControl {
        return this.getTableChildFormControl(MgControlName.ID);
    }

    get VALUE(): FormControl {
        return this.getTableChildFormControl(MgControlName.VALUE);
    }

    get LETTER(): FormControl {
        return this.getTableChildFormControl(MgControlName.LETTER);
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}