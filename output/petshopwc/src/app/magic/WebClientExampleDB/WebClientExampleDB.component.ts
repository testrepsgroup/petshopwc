import {
    Component
} from '@angular/core';

import {
    FormGroup
} from "@angular/forms";
import {
    MgFormControlsAccessor,
    MgControlName
} from "./WebClientExampleDB.mg.controls.g";


import {
    BaseMatTableMagicComponent,
    matMagicProviders
} from "@magic-xpa/angular-material-core";


@Component({
    selector: 'mga-WebClientExampleDB',
    providers: [...matMagicProviders],
    templateUrl: './WebClientExampleDB.component.html'
})
export class WebClientExampleDB extends BaseMatTableMagicComponent {

    mgc = MgControlName;
    mgfc: MgFormControlsAccessor;
    createFormControlsAccessor(formGroup: FormGroup) {
        this.mgfc = new MgFormControlsAccessor(formGroup, this.magicServices);
    }

    displayedColumns = [
        'Column5',
        'Column6',
        'Column7',
    ];

}